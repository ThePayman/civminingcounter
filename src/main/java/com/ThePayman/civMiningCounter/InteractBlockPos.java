package com.ThePayman.civMiningCounter;

import net.minecraft.util.math.BlockPos;

public class InteractBlockPos extends BlockPos {
    public InteractBlockPos (int x, int y, int z) {
        super(x, y, z);
    }
    @Override
    public boolean equals(Object obj) {
        return this.compareTo((BlockPos) obj) == 0;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 7 * hash + this.getX();
        hash = 1373 * hash + this.getY();
        hash = 23454 * hash + this.getZ();
        return hash;
    }
}
