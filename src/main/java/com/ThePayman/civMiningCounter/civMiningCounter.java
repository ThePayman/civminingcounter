package com.ThePayman.civMiningCounter;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import scala.Console;

import java.io.File;
import java.io.FileReader;
import java.util.*;

@Mod(modid = civMiningCounter.MODID, name = civMiningCounter.NAME, version = civMiningCounter.VERSION)
public class civMiningCounter {
    public static final String MODID = "civminingcounter";
    public static final String NAME = "Civ Mining Counter Mod";
    public static final String VERSION = "1.3";

    public static String file = "civMiningCounter/civMiningCounter.json";
    public static JsonObject jsonObject;

    public static HashMap<InteractBlockPos, Date> blockPosList = new HashMap<>();
    @Mod.EventHandler
    public void preInit (FMLPreInitializationEvent event) {
        Console.println(Minecraft.getMinecraft().mcDataDir);
    }
    @Mod.EventHandler
    public void init (FMLInitializationEvent event) {
        File fileO = new File(file);
        fileO.getParentFile().mkdirs();
        try {
            fileO.createNewFile();
            FileReader reader = new FileReader(file);
            JsonParser jsonParser = new JsonParser();

            JsonObject gjson = new JsonObject();
            try {
                jsonObject = (JsonObject) jsonParser.parse(reader);
            } catch (Exception e ) {
                if (jsonObject == null) jsonObject = new JsonObject();
            }
        } catch (Exception e) {

        }
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                blockPosList.forEach((blockPos, date) -> {
                    if (new Date().getTime() - date.getTime() > 5000) {
                        blockPosList.remove(blockPos);
                    }
                });
            }
        }, new Date(), 1000);
    }
    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(new civMiningCounterEventHandler());
    }
}
