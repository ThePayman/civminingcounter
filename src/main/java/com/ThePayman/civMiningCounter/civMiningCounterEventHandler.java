package com.ThePayman.civMiningCounter;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.Date;

import static com.ThePayman.civMiningCounter.civMiningCounter.blockPosList;

public class civMiningCounterEventHandler {
    @SubscribeEvent
    public void onWorldLoad(WorldEvent.Load event) {
        if (event.getWorld().isRemote) event.getWorld().addEventListener(new worldEventListener());
    }
    @SubscribeEvent
    public void onBlockInteraction(PlayerInteractEvent.LeftClickBlock event) {
        BlockPos pos = event.getPos();
        blockPosList.put(new InteractBlockPos(pos.getX(), pos.getY(), pos.getZ()), new Date());

    }
}
