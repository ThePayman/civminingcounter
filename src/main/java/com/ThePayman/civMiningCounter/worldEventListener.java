package com.ThePayman.civMiningCounter;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldEventListener;
import net.minecraft.world.World;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;

import static com.ThePayman.civMiningCounter.civMiningCounter.file;
import static com.ThePayman.civMiningCounter.civMiningCounter.jsonObject;
import static com.ThePayman.civMiningCounter.civMiningCounter.blockPosList;

public class worldEventListener implements IWorldEventListener {
    public static Minecraft MINECRAFT = Minecraft.getMinecraft();
    @Override
    public void notifyBlockUpdate(World worldIn, BlockPos pos, IBlockState oldState, IBlockState newState, int flags) {
        EntityPlayer player = worldIn.getClosestPlayer(pos.getX(), pos.getY(), pos.getZ(), 5, false);

        if (player != null && Minecraft.getMinecraft().player.getName().equals(player.getName())) {
            try {
                FileReader reader = new FileReader(file);
                JsonParser jsonParser = new JsonParser();
                try {
                    jsonObject = (JsonObject) jsonParser.parse(reader);
                } catch (Exception e) {

                }
            } catch (Exception e) {

            }
            String blockName = oldState.getBlock().getLocalizedName();

            ArrayList<String> blocksAccepted = new ArrayList<>();
            addAllowedBlocks(blocksAccepted);

            InteractBlockPos interactBlockPos = new InteractBlockPos(pos.getX(), pos.getY(), pos.getZ());

            if (blocksAccepted.contains(blockName) && newState.getBlock().getLocalizedName().equals("Air") && hasRecentBlockInteraction(interactBlockPos)) {
                String worldName = getWorldName(worldIn, pos);
                String playerName =  Minecraft.getMinecraft().player.getDisplayName().getUnformattedText();
                String biome = worldIn.getBiome(pos).getBiomeName();

                if (!jsonObject.has(worldName)) {
                    jsonObject.add(worldName, new JsonObject());
                }

                JsonObject worldObject = (JsonObject) jsonObject.get(worldName);

                if (!worldObject.has(playerName)) {
                    worldObject.add(playerName, new JsonObject());
                }

                JsonObject playerObject = (JsonObject) worldObject.get(playerName);

                if (!playerObject.has(biome)) {
                    playerObject.add(biome, new JsonObject());
                }

                JsonObject biomeObject = (JsonObject) playerObject.get(biome);

                if (!biomeObject.has(blockName)) {
                    biomeObject.addProperty(blockName, 0);
                }

                long amount = biomeObject.get(blockName).getAsLong();
                biomeObject.addProperty(blockName, amount + 1);

                try {
                    FileWriter fileWriter = new FileWriter(file);
                    fileWriter.write(jsonObject.toString());
                    fileWriter.close();
                } catch (Exception e) {

                }

            }
        }
        MINECRAFT.renderGlobal.notifyBlockUpdate(worldIn, pos, oldState, newState, flags);
    }

    public boolean hasRecentBlockInteraction (InteractBlockPos pos) {
        if (blockPosList.containsKey(pos)) {
            Date date = blockPosList.get(pos);
            if (new Date().getTime() - date.getTime() < 5000) {
                blockPosList.remove(pos);
                return true;
            }
        }
        return false;
    }

    public void addAllowedBlocks (ArrayList<String> blocksAccepted) {
        blocksAccepted.add("Stone");
        blocksAccepted.add("Diamond Ore");
        blocksAccepted.add("Iron Ore");
        blocksAccepted.add("Coal Ore");
        blocksAccepted.add("Emerald Ore");
        blocksAccepted.add("Nether Quartz Ore");
        blocksAccepted.add("Gold Ore");
        blocksAccepted.add("Redstone Ore");
        blocksAccepted.add("Lapis Lazuli Ore");
        blocksAccepted.add("Block of Coal");
    }
    public String getWorldName (World worldIn, BlockPos pos) {
        String worldName;
        try {
            worldName = Minecraft.getMinecraft().getCurrentServerData().serverIP;
            if (worldIn.getBlockState( new BlockPos(pos.getX(), 255, pos.getZ())).getBlock().getLocalizedName().equals("Bedrock")) {
                worldName += "-AN";
            } else {
                worldName += "-overworld";
            }

        } catch (Exception e) {
            worldName = "local";
        }
        return worldName;
    }




    @Override
    public void notifyLightSet(BlockPos pos)
    {
        MINECRAFT.renderGlobal.notifyLightSet(pos);
    }
    @Override
    public void markBlockRangeForRenderUpdate(int x1, int y1, int z1, int x2, int y2, int z2)
    {
        MINECRAFT.renderGlobal.markBlockRangeForRenderUpdate(x1, y1, z1, x2, y2, z2);
    }
    @Override
    public void playSoundToAllNearExcept(EntityPlayer player, SoundEvent soundIn, SoundCategory category, double x, double y, double z, float volume, float pitch)
    {
      MINECRAFT.renderGlobal.playSoundToAllNearExcept(player, soundIn, category, x, y, z, volume, pitch);
    }

    @Override
    public void playRecord(SoundEvent soundIn, BlockPos pos)
    {
        MINECRAFT.renderGlobal.playRecord(soundIn, pos);
    }

    @Override
    public void spawnParticle(int particleID, boolean ignoreRange, double xCoord, double yCoord, double zCoord, double xSpeed, double ySpeed, double zSpeed, int... parameters)
    {

        MINECRAFT.renderGlobal.spawnParticle(particleID, ignoreRange, xCoord, yCoord, zCoord, xSpeed, ySpeed, xSpeed, parameters);
    }

    @Override
    public void spawnParticle(int id, boolean ignoreRange, boolean p_190570_3_, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed, int... parameters)
    {

      MINECRAFT.renderGlobal.spawnParticle(id, ignoreRange, x, y, z, xSpeed, ySpeed, xSpeed, parameters);
    }

    @Override
    public void onEntityAdded(Entity entityIn)
    {
      MINECRAFT.renderGlobal.onEntityAdded(entityIn);
    }

    @Override
    public void onEntityRemoved(Entity entityIn)
    {
        MINECRAFT.renderGlobal.onEntityRemoved(entityIn);
    }

    @Override
    public void broadcastSound(int soundID, BlockPos pos, int data)
    {
      MINECRAFT.renderGlobal.broadcastSound(soundID, pos, data);
    }

    @Override
    public void playEvent(EntityPlayer player, int type, BlockPos blockPosIn, int data)
    {
        MINECRAFT.renderGlobal.playEvent(player, type, blockPosIn, data);
    }

    @Override
    public void sendBlockBreakProgress(int breakerId, BlockPos pos, int progress)
    {
        MINECRAFT.renderGlobal.sendBlockBreakProgress(breakerId, pos, progress);
    }
}
